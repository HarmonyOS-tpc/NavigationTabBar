/*
 * Copyright (C) 2015 Basil Miller
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package devlight.io.library;

import devlight.io.library.ResourceTable;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Texture;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.ColorFilter;
import ohos.agp.render.BlendMode;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;
import java.security.SecureRandom;

import static ohos.agp.render.BlendMode.SRC_IN;
import static ohos.agp.render.Canvas.PorterDuffMode.CLEAR;
import static ohos.agp.utils.TextAlignment.CENTER;

public class NavigationTabBar extends Component implements Component.TouchEventListener
        , Component.DrawTask, PageSlider.PageChangedListener {

    // NTB constants
    protected static Paint mDefaultPaint = new Paint() {
        {
            setAntiAlias(true);
            setDither(true);
            setFilterBitmap(true);
        }
    };
    private static final String TAG = NavigationTabBar.class.getSimpleName();
    protected final static String PREVIEW_BADGE = "0";
    protected final static String PREVIEW_TITLE = "Title";

    protected final static int INVALID_INDEX = -1;
    public final static int AUTO_SIZE = -2;
    public final static int AUTO_COLOR = -3;
    public final static int AUTO_SCALE = -4;

    protected final static int DEFAULT_BADGE_ANIMATION_DURATION = 200;
    protected final static int DEFAULT_BADGE_REFRESH_ANIMATION_DURATION = 100;
    protected final static int DEFAULT_ANIMATION_DURATION = 300;
    protected final static float DEFAULT_ICON_SIZE_FRACTION = 0.5F;
    protected final static float DEFAULT_TITLE_ICON_SIZE_FRACTION = 0.5F;

    protected final static int DEFAULT_INACTIVE_COLOR = Color.BLACK.getValue();
    protected final static int DEFAULT_ACTIVE_COLOR = Color.WHITE.getValue();
    protected final static int DEFAULT_BG_COLOR = Color.LTGRAY.getValue();

    protected final static float MIN_FRACTION = 0.0F;
    protected final static float MAX_FRACTION = 1.0F;

    protected final static int MIN_ALPHA = 0;
    protected final static int MAX_ALPHA = 255;

    protected final static float SCALED_FRACTION = 0.3F;
    protected final static float TITLE_ACTIVE_ICON_SCALE_BY = 0.2F;
    protected final static float TITLE_ACTIVE_SCALE_BY = 0.2F;
    protected final static float TITLE_SIZE_FRACTION = 0.2F;
    protected final static float TITLE_MARGIN_FRACTION = 0.15F;
    protected final static float TITLE_MARGIN_SCALE_FRACTION = 0.25F;

    protected final static float BADGE_HORIZONTAL_FRACTION = 0.5F;
    protected final static float BADGE_VERTICAL_FRACTION = 0.75F;
    protected final static float BADGE_TITLE_SIZE_FRACTION = 0.9F;

    protected final static float LEFT_FRACTION = 0.25F;
    protected final static float CENTER_FRACTION = 0.5F;
    protected final static float RIGHT_FRACTION = 0.75F;

    // NTB and pointer bounds
    protected final RectF mBounds = new RectF();
    protected final RectF mBgBounds = new RectF();
    protected final RectF mPointerBounds = new RectF();
    // Badge bounds and bg badge bounds
    protected final Rect mBadgeBounds = new Rect();
    protected final RectF mBgBadgeBounds = new RectF();

    // Canvas, where all of other canvas will be merged
    protected PixelMap mBitmap;
    protected final Canvas mCanvas = new Canvas();

    // Canvas with icons
    protected PixelMap mIconsBitmap;
    protected final Canvas mIconsCanvas = new Canvas();

    // Canvas with titles
    protected PixelMap mTitlesBitmap;
    protected final Canvas mTitlesCanvas = new Canvas();

    // Canvas for our rect pointer
    protected PixelMap mPointerBitmap;
    protected final Canvas mPointerCanvas = new Canvas();

    // Detect if behavior already set
    protected boolean mIsBehaviorSet;
    // Detect if behavior enabled
    protected boolean mBehaviorEnabled;

    // Main paint
    protected final Paint mPaint = new Paint(mDefaultPaint) {
        {
            setStyle(Style.FILL_STYLE);
        }
    };
    // Background color paint
    protected final Paint mBgPaint = new Paint(mDefaultPaint) {
        {
            setStyle(Style.FILL_STYLE);
        }
    };
    // Pointer paint
    protected final Paint mPointerPaint = new Paint(mDefaultPaint) {
        {
            setBlendMode(BlendMode.DST_IN);
        }
    };

    // Icons paint
    protected final Paint mIconPaint = new Paint(mDefaultPaint);
    protected final Paint mSelectedIconPaint = new Paint(mDefaultPaint);

    // Paint for icon mask pointer
    protected final Paint mIconPointerPaint = new Paint(mDefaultPaint) {
        {
            setStyle(Style.FILL_STYLE);
            setBlendMode(SRC_IN);
        }
    };

    // Paint for model title
    protected final Paint mModelTitlePaint = new Paint(mDefaultPaint) {
        {
            setColor(Color.WHITE);
            setTextAlign(TextAlignment.CENTER);
        }
    };

    // Paint for badge
    protected final Paint mBadgePaint = new Paint(mDefaultPaint) {
        {
            setTextAlign(CENTER);
            setFakeBoldText(true);
        }
    };

    // Variables for animator
    protected final AnimatorValue mAnimator = new AnimatorValue();
    protected final ResizeInterpolator mResizeInterpolator = new ResizeInterpolator();
    protected int mAnimationDuration;

    // NTB models
    protected final List<Model> mModels = new ArrayList<>();

    // Variables for ViewPager
    protected PageSlider mViewPager;
    protected PageSlider.PageChangedListener mOnPageChangeListener;
    protected int mScrollState;

    // Tab listener
    protected OnTabBarSelectedIndexListener mOnTabBarSelectedIndexListener;
    protected AnimatorValue.StateChangedListener mAnimatorListener;

    // Variables for sizes
    protected float mModelSize;
    protected float mIconSize;
    protected float mIconSizeFraction;
    // Corners radius for rect mode
    protected float mCornersRadius;

    // Model title size and margin
    protected float mModelTitleSize = AUTO_SIZE;
    protected float mTitleMargin;

    // Model badge title size and margin
    protected float mBadgeMargin;
    protected float mBadgeTitleSize = AUTO_SIZE;

    // Model title mode: active ar all
    protected TitleMode mTitleMode;
    // Model badge position: left, center or right
    protected BadgePosition mBadgePosition;
    // Model badge gravity: top or bottom
    protected BadgeGravity mBadgeGravity;

    // Model badge bg and title color.
    // By default badge bg color is the active model color and badge title color is the model bg color
    // To reset colors just set bg and title color to AUTO_COLOR
    protected int mBadgeTitleColor = AUTO_COLOR;
    protected int mBadgeBgColor = AUTO_COLOR;

    // Indexes
    protected int mLastIndex = INVALID_INDEX;
    protected int mIndex = INVALID_INDEX;
    // General fraction value
    protected float mFraction;

    // Coordinates of pointer
    protected float mStartPointerX;
    protected float mEndPointerX;
    protected float mPointerLeftTop;
    protected float mPointerRightBottom;

    // Detect if model has title
    protected boolean mIsTitled;
    // Detect if model has badge
    protected boolean mIsBadged;
    // Detect if model icon scaled
    protected boolean mIsScaled;
    // Detect if model icon tinted
    protected boolean mIsTinted;
    // Detect if model can swiped
    protected boolean mIsSwiped;
    // Detect if model badge have custom typeface
    protected boolean mIsBadgeUseTypeface;
    // Detect if is bar mode or indicator pager mode
    protected boolean mIsViewPagerMode;
    // Detect whether the horizontal orientation
    protected boolean mIsHorizontalOrientation;
    // Detect if we move from left to right
    protected boolean mIsResizeIn;
    // Detect if we get action down event
    protected boolean mIsActionDown;
    // Detect if we get action down event on pointer
    protected boolean mIsPointerActionDown;
    // Detect when we set index from tab bar nor from ViewPager
    protected boolean mIsSetIndexFromTabBar;

    // Color variables
    protected int mInactiveColor;
    protected int mActiveColor;
    protected int mBgColor;

    // Custom typeface
    protected Font mTypeface;
    protected Context mContext;

    //custom attributes
    private static final String ntb_titled = "ntb_titled";
    private static final String ntb_badged = "ntb_badged";
    private static final String ntb_scaled = "ntb_scaled";
    private static final String ntb_tinted = "ntb_tinted";
    private static final String ntb_swiped = "ntb_swiped";
    private static final String ntb_badge_use_typeface = "ntb_badge_use_typeface";
    private static final String ntb_title_size = "ntb_title_size";
    private static final String ntb_title_mode = "ntb_title_mode";
    private static final String ntb_badge_size = "ntb_badge_size";
    private static final String ntb_badge_position = "ntb_badge_position";
    private static final String ntb_badge_gravity = "ntb_badge_gravity";
    private static final String ntb_badge_bg_color = "ntb_badge_bg_color";
    private static final String ntb_badge_title_color = "ntb_badge_title_color";
    private static final String ntb_typeface = "ntb_typeface";
    private static final String ntb_corners_radius = "ntb_corners_radius";
    private static final String ntb_icon_size_fraction = "ntb_icon_size_fraction";
    private static final String ntb_animation_duration = "ntb_animation_duration";
    private static final String ntb_inactive_color = "ntb_inactive_color";
    private static final String ntb_active_color = "ntb_active_color";
    private static final String ntb_bg_color = "ntb_bg_color";

    public NavigationTabBar(final Context context) {
        this(context, null);
    }

    public NavigationTabBar(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public NavigationTabBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
        mContext = context;

        //Init NTB
        try {

            setIsTitled(attrSet.getAttr(ntb_titled).isPresent() ?
                    attrSet.getAttr(ntb_titled).get().getBoolValue() : false);
            setIsBadged(attrSet.getAttr(ntb_badged).isPresent() ?
                    attrSet.getAttr(ntb_badged).get().getBoolValue() : false);
            setIsScaled(attrSet.getAttr(ntb_scaled).isPresent() ?
                    attrSet.getAttr(ntb_scaled).get().getBoolValue() : true);
            setIsTinted(attrSet.getAttr(ntb_tinted).isPresent() ?
                    attrSet.getAttr(ntb_tinted).get().getBoolValue() : true);
            setIsSwiped(attrSet.getAttr(ntb_swiped).isPresent() ?
                    attrSet.getAttr(ntb_swiped).get().getBoolValue() : true);
            setTitleSize(attrSet.getAttr(ntb_title_size).isPresent() ?
                    attrSet.getAttr(ntb_title_size).get().getDimensionValue() : AUTO_SIZE);
            setIsBadgeUseTypeface(attrSet.getAttr(ntb_badge_use_typeface).isPresent() ?
                    attrSet.getAttr(ntb_badge_use_typeface).get().getBoolValue() : false);
            setTitleMode(attrSet.getAttr(ntb_title_mode).isPresent() ?
                    attrSet.getAttr(ntb_title_mode).get().getIntegerValue() : TitleMode.ALL_INDEX);
            setBadgeSize(attrSet.getAttr(ntb_badge_size).isPresent() ?
                    attrSet.getAttr(ntb_badge_size).get().getDimensionValue() : AUTO_SIZE);
            setBadgePosition(attrSet.getAttr(ntb_badge_position).isPresent() ?
                    attrSet.getAttr(ntb_badge_position).get().getIntegerValue() : BadgePosition.RIGHT_INDEX);
            setBadgeGravity(attrSet.getAttr(ntb_badge_gravity).isPresent() ?
                    attrSet.getAttr(ntb_badge_gravity).get().getIntegerValue() : BadgeGravity.TOP_INDEX);
            setBadgeBgColor(attrSet.getAttr(ntb_badge_bg_color).isPresent() ?
                    attrSet.getAttr(ntb_badge_bg_color).get().getColorValue().getValue() : AUTO_COLOR);
            setBadgeTitleColor(attrSet.getAttr(ntb_badge_title_color).isPresent() ?
                    attrSet.getAttr(ntb_badge_title_color).get().getColorValue().getValue() : AUTO_COLOR);
            setTypeface(attrSet.getAttr(ntb_typeface).isPresent() ?
                    attrSet.getAttr(ntb_typeface).get().getStringValue() : "Aaargh.ttf");
            setInactiveColor(attrSet.getAttr(ntb_inactive_color).isPresent() ?
                    attrSet.getAttr(ntb_inactive_color).get().getColorValue().getValue() : DEFAULT_INACTIVE_COLOR);
            setActiveColor(attrSet.getAttr(ntb_active_color).isPresent() ?
                    attrSet.getAttr(ntb_active_color).get().getColorValue().getValue() : DEFAULT_ACTIVE_COLOR);
            setBgColor(attrSet.getAttr(ntb_bg_color).isPresent() ?
                    attrSet.getAttr(ntb_bg_color).get().getColorValue().getValue() : DEFAULT_BG_COLOR);
            setAnimationDuration(attrSet.getAttr(ntb_animation_duration).isPresent() ?
                    attrSet.getAttr(ntb_animation_duration).get().getIntegerValue() : DEFAULT_ANIMATION_DURATION);
            setCornersRadius(attrSet.getAttr(ntb_corners_radius).isPresent() ?
                    attrSet.getAttr(ntb_corners_radius).get().getDimensionValue() : 0.0F);
            setIconSizeFraction(attrSet.getAttr(ntb_icon_size_fraction).isPresent() ?
                    attrSet.getAttr(ntb_icon_size_fraction).get().getFloatValue() : AUTO_SCALE);


            // Init animator
            mAnimator.setCurveType(Animator.CurveType.LINEAR);
            mAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    updateIndicatorPosition(v);
                }
            });

            // Set preview models
            // Get preview colors
            String[] previewColors = ResUtil.getStringArray(context, ResourceTable.Strarray_default_preview);
            for (String previewColor : previewColors)
                mModels.add(new Model.Builder(null, Color.getIntColor(previewColor)).build());
            postLayout();

        } finally {

        }

    }

    public int getAnimationDuration() {
        return mAnimationDuration;
    }

    public void setAnimationDuration(final int animationDuration) {
        mAnimationDuration = animationDuration;
        mAnimator.setDuration(mAnimationDuration);
        resetScroller();
    }

    public List<Model> getModels() {
        return mModels;
    }

    public void setModels(final List<Model> models) {
        //Set update listeners to badge model animation
        for (final Model model : models) {
            model.mBadgeAnimator.setValueUpdateListener(null);
            model.mBadgeAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    model.mBadgeFraction = v;
                    invalidate();
                }
            });
        }

        mModels.clear();
        mModels.addAll(models);
        postLayout();
    }

    public boolean isTitled() {
        return mIsTitled;
    }

    public void setIsTitled(final boolean isTitled) {
        mIsTitled = isTitled;
        postLayout();
    }

    public boolean isBadged() {
        return mIsBadged;
    }

    public void setIsBadged(final boolean isBadged) {
        mIsBadged = isBadged;
        postLayout();
    }

    public boolean isScaled() {
        return mIsScaled;
    }

    public void setIsScaled(final boolean isScaled) {
        mIsScaled = isScaled;
        postLayout();
    }

    public boolean isTinted() {
        return mIsTinted;
    }

    public void setIsTinted(final boolean isTinted) {
        mIsTinted = isTinted;
        updateTint();
    }

    public boolean isSwiped() {
        return mIsSwiped;
    }

    public void setIsSwiped(final boolean swiped) {
        mIsSwiped = swiped;
    }

    public float getTitleSize() {
        return mModelTitleSize;
    }

    // To reset title size to automatic just put in method AUTO_SIZE value
    public void setTitleSize(final float modelTitleSize) {
        mModelTitleSize = modelTitleSize;
        if (modelTitleSize == AUTO_SIZE) postLayout();
    }

    public boolean isBadgeUseTypeface() {
        return mIsBadgeUseTypeface;
    }

    public void setIsBadgeUseTypeface(final boolean isBadgeUseTypeface) {
        mIsBadgeUseTypeface = isBadgeUseTypeface;
        setBadgeTypeface();
        invalidate();
    }

    public TitleMode getTitleMode() {
        return mTitleMode;
    }

    protected void setTitleMode(final int index) {
        switch (index) {
            case TitleMode.ACTIVE_INDEX:
                setTitleMode(TitleMode.ACTIVE);
                break;
            case TitleMode.ALL_INDEX:
            default:
                setTitleMode(TitleMode.ALL);
                break;
        }
    }

    public void setTitleMode(final TitleMode titleMode) {
        mTitleMode = titleMode;
        invalidate();
    }

    public BadgePosition getBadgePosition() {
        return mBadgePosition;
    }

    protected void setBadgePosition(final int index) {
        switch (index) {
            case BadgePosition.LEFT_INDEX:
                setBadgePosition(BadgePosition.LEFT);
                break;
            case BadgePosition.CENTER_INDEX:
                setBadgePosition(BadgePosition.CENTER);
                break;
            case BadgePosition.RIGHT_INDEX:
            default:
                setBadgePosition(BadgePosition.RIGHT);
                break;
        }
    }

    public void setBadgePosition(final BadgePosition badgePosition) {
        mBadgePosition = badgePosition;
        invalidate();
    }

    public BadgeGravity getBadgeGravity() {
        return mBadgeGravity;
    }

    protected void setBadgeGravity(final int index) {
        switch (index) {
            case BadgeGravity.BOTTOM_INDEX:
                setBadgeGravity(BadgeGravity.BOTTOM);
                break;
            case BadgeGravity.TOP_INDEX:
            default:
                setBadgeGravity(BadgeGravity.TOP);
                break;
        }
    }

    public void setBadgeGravity(final BadgeGravity badgeGravity) {
        mBadgeGravity = badgeGravity;
        postLayout();
    }

    public int getBadgeBgColor() {
        return mBadgeBgColor;
    }

    public void setBadgeBgColor(final int badgeBgColor) {
        mBadgeBgColor = badgeBgColor;
    }

    public int getBadgeTitleColor() {
        return mBadgeTitleColor;
    }

    public void setBadgeTitleColor(final int badgeTitleColor) {
        mBadgeTitleColor = badgeTitleColor;
    }

    public float getBadgeSize() {
        return mBadgeTitleSize;
    }

    // To reset badge title size to automatic just put in method AUTO_SIZE value
    public void setBadgeSize(final float badgeTitleSize) {
        mBadgeTitleSize = badgeTitleSize;
        if (mBadgeTitleSize == AUTO_SIZE) postLayout();
    }

    public Font getTypeface() {
        return mTypeface;
    }

    public void setTypeface(final String typeface) {
        if (TextUtils.isEmpty(typeface)) return;

        Font tempTypeface;
        try {
            tempTypeface = ResUtil.createFont(mContext, typeface);
        } catch (Exception e) {
            tempTypeface = Font.DEFAULT;
            e.printStackTrace();
        }

        setTypeface(tempTypeface);
    }

    private boolean isEmpty(String string) {
        return string == null || string.length() == 0;
    }

    public void setTypeface(final Font typeface) {
        mTypeface = typeface;
        mModelTitlePaint.setFont(typeface);
        setBadgeTypeface();
        invalidate();
    }

    protected void setBadgeTypeface() {
        mBadgePaint.setFont(
                mIsBadgeUseTypeface ? mTypeface : Font.DEFAULT
        );
    }

    public int getActiveColor() {
        return mActiveColor;
    }

    public void setActiveColor(final int activeColor) {
        mActiveColor = activeColor;

        // Set icon pointer active color
        mIconPointerPaint.setColor(new Color(mActiveColor));
        updateTint();
    }

    public int getInactiveColor() {
        return mInactiveColor;
    }

    public void setInactiveColor(final int inactiveColor) {
        mInactiveColor = inactiveColor;

        // Set inactive color to title
        mModelTitlePaint.setColor(new Color(mInactiveColor));
        updateTint();
    }

    public int getBgColor() {
        return mBgColor;
    }

    public void setBgColor(final int bgColor) {
        mBgColor = bgColor;
        mBgPaint.setColor(new Color(mBgColor));
        invalidate();
    }

    public float getCornersRadius() {
        return mCornersRadius;
    }

    public void setCornersRadius(final float cornersRadius) {
        mCornersRadius = cornersRadius;
        invalidate();
    }

    public float getIconSizeFraction() {
        return mIconSizeFraction;
    }

    // To reset scale fraction of icon to automatic just put in method AUTO_SCALE value
    public void setIconSizeFraction(final float iconSizeFraction) {
        mIconSizeFraction = iconSizeFraction;
        postLayout();
    }

    public float getBadgeMargin() {
        return mBadgeMargin;
    }

    public float getBarHeight() {
        return mBounds.height();
    }

    public OnTabBarSelectedIndexListener getOnTabBarSelectedIndexListener() {
        return mOnTabBarSelectedIndexListener;
    }

    // Set on tab bar selected index listener where you can trigger action onStart or onEnd
    public void setOnTabBarSelectedIndexListener(
            final OnTabBarSelectedIndexListener onTabBarSelectedIndexListener
    ) {
        mOnTabBarSelectedIndexListener = onTabBarSelectedIndexListener;

        if (mAnimatorListener == null)
            mAnimatorListener = new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                    if (mOnTabBarSelectedIndexListener != null)
                        mOnTabBarSelectedIndexListener.onStartTabSelected(
                                mModels.get(mIndex), mIndex
                        );

                    ((AnimatorValue) animator).setStateChangedListener(null);
                    ((AnimatorValue) animator).setStateChangedListener(this);
                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    if (mIsViewPagerMode) return;

                    ((AnimatorValue) animator).setStateChangedListener(null);
                    ((AnimatorValue) animator).setStateChangedListener(this);

                    if (mOnTabBarSelectedIndexListener != null)
                        mOnTabBarSelectedIndexListener.onEndTabSelected(
                                mModels.get(mIndex), mIndex
                        );
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            };
        mAnimator.setStateChangedListener(null);
        mAnimator.setStateChangedListener(mAnimatorListener);
    }

    public void setViewPager(final PageSlider viewPager) {
        // Detect whether ViewPager mode
        if (viewPager == null) {
            mIsViewPagerMode = false;
            return;
        }

        if (viewPager.equals(mViewPager)) return;
        if (mViewPager != null) //noinspection deprecation
            mViewPager.addPageChangedListener(null);
        if (viewPager.getProvider() == null)
            throw new IllegalStateException("ViewPager does not provide adapter instance.");

        mIsViewPagerMode = true;
        mViewPager = viewPager;
        mViewPager.addPageChangedListener(this);

        resetScroller();
        invalidate();
    }

    public void setViewPager(final PageSlider viewPager, int index) {
        setViewPager(viewPager);

        mIndex = index;
        if (mIsViewPagerMode) mViewPager.setCurrentPage(index, false);
        invalidate();
    }

    // Reset scroller and reset scroll duration equals to animation duration
    protected void resetScroller() {
        if (mViewPager == null) return;
    }

    public void setOnPageChangeListener(final PageSlider.PageChangedListener listener) {
        mOnPageChangeListener = listener;
    }

    // Return if the behavior translation is enabled
    public boolean isBehaviorEnabled() {
        return mBehaviorEnabled;
    }

    // Set the behavior translation value
    public void setBehaviorEnabled(final boolean enabled) {
        mBehaviorEnabled = enabled;
    }

    public int getModelIndex() {
        return mIndex;
    }

    public void setModelIndex(int index) {
        setModelIndex(index, false);
    }

    // Set model index from touch or programmatically
    public void setModelIndex(final int modelIndex, final boolean isForce) {
        if (mAnimator.isRunning()) return;
        if (mModels.isEmpty()) return;

        int index = modelIndex;
        boolean force = isForce;

        // This check gives us opportunity to have an non selected model
        if (mIndex == INVALID_INDEX) force = true;
        // Detect if last is the same
        if (index == mIndex) force = true;
        // Snap index to models size
        index = Math.max(0, Math.min(index, mModels.size() - 1));

        mIsResizeIn = index < mIndex;
        mLastIndex = mIndex;
        mIndex = index;

        mIsSetIndexFromTabBar = true;
        if (mIsViewPagerMode) {
            if (mViewPager == null) throw new IllegalStateException("ViewPager is null.");
            mViewPager.setCurrentPage(index, false);
        }

        // Set startX and endX for animation,
        // where we animate two sides of rect with different interpolation
        if (force) {
            mStartPointerX = mIndex * mModelSize;
            mEndPointerX = mStartPointerX;
        } else {
            mStartPointerX = mPointerLeftTop;
            mEndPointerX = mIndex * mModelSize;
        }

        // If it force, so update immediately, else animate
        // This happens if we set index onCreate or something like this
        // You can use force param or call this method in some post()
        if (force) {
            updateIndicatorPosition(MAX_FRACTION);

            if (mOnTabBarSelectedIndexListener != null)
                mOnTabBarSelectedIndexListener.onStartTabSelected(mModels.get(mIndex), mIndex);

            // Force onPageScrolled listener and refresh VP
            if (mIsViewPagerMode) {
            } else {
                if (mOnTabBarSelectedIndexListener != null)
                    mOnTabBarSelectedIndexListener.onEndTabSelected(mModels.get(mIndex), mIndex);
            }
        } else mAnimator.start();
    }

    // Deselect active index and reset pointer
    public void deselect() {
        mLastIndex = INVALID_INDEX;
        mIndex = INVALID_INDEX;
        mStartPointerX = INVALID_INDEX * mModelSize;
        mEndPointerX = mStartPointerX;
        updateIndicatorPosition(MIN_FRACTION);
    }

    protected void updateIndicatorPosition(final float fraction) {
        // Update general fraction
        mFraction = fraction;

        // Set the pointer left top side coordinate
        mPointerLeftTop = mStartPointerX +
                (mResizeInterpolator.getResizeInterpolation(fraction, mIsResizeIn) *
                        (mEndPointerX - mStartPointerX));
        // Set the pointer right bottom side coordinate
        mPointerRightBottom = (mStartPointerX + mModelSize) +
                (mResizeInterpolator.getResizeInterpolation(fraction, !mIsResizeIn) *
                        (mEndPointerX - mStartPointerX));

        // Update pointer
        invalidate();
    }

    // Update NTB
    protected void notifyDataSetChanged() {
        postLayout();
        invalidate();
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        // Return if animation is running
        if (mAnimator.isRunning()) return true;
        // If is not idle state, return
        if (mScrollState != PageSlider.SLIDING_STATE_IDLE) return true;
        final int action = touchEvent.getAction();
        final int activePointerIndex = touchEvent.getIndex();
        MmiPoint point = touchEvent.getPointerPosition(activePointerIndex);
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                // Action down touch
                mIsActionDown = true;
                if (!mIsViewPagerMode) break;
                if (!mIsSwiped) break;
                // Detect if we touch down on pointer, later to move
                if (mIsHorizontalOrientation)
                    mIsPointerActionDown = (int) (point.getX() / mModelSize) == mIndex;
                else
                    mIsPointerActionDown = (int) (point.getY() / mModelSize) == mIndex;
                break;
            case TouchEvent.POINT_MOVE:
                // If pointer touched, so move
                if (mIsPointerActionDown) {
                    if (mIsHorizontalOrientation)
                        mViewPager.setCurrentPage((int) (point.getX() / mModelSize), true);
                    else
                        mViewPager.setCurrentPage((int) (point.getY() / mModelSize), true);
                    break;
                }
                if (mIsActionDown) break;
            case TouchEvent.PRIMARY_POINT_UP:
                // Press up and set model index relative to current coordinate
                if (mIsActionDown) {
                    if (mIsHorizontalOrientation) setModelIndex((int) (point.getX() / mModelSize));
                    else setModelIndex((int) (point.getY() / mModelSize));
                }
            case TouchEvent.CANCEL:
            case TouchEvent.NONE:
            default:
                // Reset action touch variables
                mIsPointerActionDown = false;
                mIsActionDown = false;
                break;
        }

        return true;
    }

    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        // Get measure size
        final int width = EstimateSpec.getSize(widthMeasureSpec);
        final int height = EstimateSpec.getSize(heightMeasureSpec);

        if (mModels.isEmpty() || width == 0 || height == 0) return;

        // Detect orientation and calculate icon size
        if (width > height) {
            mIsHorizontalOrientation = true;

            // Get model size
            mModelSize = (float) width / (float) mModels.size();
            // Get smaller side
            float side = mModelSize > height ? height : mModelSize;
            if (mIsBadged) side -= side * TITLE_SIZE_FRACTION;

            mIconSize = side * (mIconSizeFraction != AUTO_SCALE ? mIconSizeFraction :
                    (mIsTitled ? DEFAULT_TITLE_ICON_SIZE_FRACTION : DEFAULT_ICON_SIZE_FRACTION));
            if (mModelTitleSize == AUTO_SIZE) mModelTitleSize = side * TITLE_SIZE_FRACTION;
            mTitleMargin = side * TITLE_MARGIN_FRACTION;

            // If is badged mode, so get vars and set paint with default bounds
            if (mIsBadged) {
                if (mBadgeTitleSize == AUTO_SIZE)
                    mBadgeTitleSize = (side * TITLE_SIZE_FRACTION) * BADGE_TITLE_SIZE_FRACTION;

                final Rect badgeBounds = new Rect();
                mBadgePaint.setTextSize((int) mBadgeTitleSize);
                Rect rect = mBadgePaint.getTextBounds(PREVIEW_BADGE);
                badgeBounds.set(rect.left, rect.top, rect.right, rect.bottom);
                mBadgeMargin = (badgeBounds.getHeight() * 0.5F) +
                        (mBadgeTitleSize * BADGE_HORIZONTAL_FRACTION * BADGE_VERTICAL_FRACTION);
            }

            if( 0f == mPointerLeftTop && 0f == mPointerRightBottom) {
                mPointerLeftTop = mModelSize * mIndex;
                mPointerRightBottom = mPointerLeftTop + mModelSize;
            }

        } else {
            // Disable vertical translation in coordinator layout
            mBehaviorEnabled = false;
            // Disable other features
            mIsHorizontalOrientation = false;
//            mIsTitled = false;
            mIsBadged = false;

            mModelSize = (float) height / (float) mModels.size();
            // Get smaller side
            float side = mModelSize > width ? width : mModelSize;

            mIconSize = (int) (side *
                    (mIconSizeFraction == AUTO_SCALE ?
                            DEFAULT_ICON_SIZE_FRACTION : mIconSizeFraction));

            if (mModelTitleSize == AUTO_SIZE) mModelTitleSize = side * TITLE_SIZE_FRACTION;
            mTitleMargin = side * TITLE_MARGIN_FRACTION;
            if( 0f == mPointerLeftTop && 0f == mPointerRightBottom) {
                mPointerLeftTop = mModelSize * mIndex;
                mPointerRightBottom = mPointerLeftTop + mModelSize;
            }
        }

        // Set bounds for NTB
        RectF ntbBounds = new RectF(0.0F, 0.0F, width, height - mBadgeMargin);
        mBounds.set(ntbBounds);

        final float barBadgeMargin = mBadgeGravity == BadgeGravity.TOP ? mBadgeMargin : 0.0F;
        RectF bounds = new RectF(0.0F, barBadgeMargin, mBounds.width(), mBounds.height() + barBadgeMargin);
        mBgBounds.set(bounds);

        // Set scale fraction for icons
        for (Model model : mModels) {
            int iconHeight = model.mIcon.getImageInfo().size.height;
            int iconWidth = model.mIcon.getImageInfo().size.width;
            final float originalIconSize = iconWidth > iconHeight ?
                    iconWidth : iconHeight;
            model.mInactiveIconScale = mIconSize / originalIconSize;
            model.mActiveIconScaleBy = model.mInactiveIconScale *
                    (mIsTitled ? TITLE_ACTIVE_ICON_SCALE_BY : SCALED_FRACTION);
        }

        // Reset bitmap to init it onDraw()
        mBitmap = null;
        mPointerBitmap = null;
        mIconsBitmap = null;
        if (mIsTitled) mTitlesBitmap = null;

        // Set start position of pointer for preview or on start
        if (!mIsViewPagerMode) {
            mIsSetIndexFromTabBar = true;

            // Set random in preview mode
            mIndex = new SecureRandom().nextInt(mModels.size());

            if (mIsBadged)
                for (int i = 0; i < mModels.size(); i++) {
                    final Model model = mModels.get(i);

                    if (i == mIndex) {
                        model.mBadgeFraction = MAX_FRACTION;
                        model.showBadge();
                    } else {
                        model.mBadgeFraction = MIN_FRACTION;
                        model.hideBadge();
                    }
                }

            mStartPointerX = mIndex * mModelSize;
            mEndPointerX = mStartPointerX;
            updateIndicatorPosition(MAX_FRACTION);
        }

        //The translation behavior has to be set up after the super.onMeasure has been called
        if (!mIsBehaviorSet) {
            setBehaviorEnabled(mBehaviorEnabled);
            mIsBehaviorSet = true;
        }
    }

    protected void updateTint() {
        if (mIsTinted) {
            // Set color filter to wrap icons with inactive color
            final ColorFilter colorFilter =
                    new ColorFilter(mInactiveColor, SRC_IN);
            mIconPaint.setColorFilter(colorFilter);
            mSelectedIconPaint.setColorFilter(colorFilter);
        } else {
            // Reset active and inactive colors
            mIconPaint.reset();
            mSelectedIconPaint.reset();
        }

        invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        // Get height of NTB with badge on nor
        onMeasure(getWidth(), getHeight());
        final int mBadgedHeight = (int) (mBounds.height() + mBadgeMargin);
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        initializationOptions.size = new Size((int) mBounds.width(), mBadgedHeight);
        // Set main canvas
        if (mBitmap == null || mBitmap.isReleased()) {
            mBitmap = PixelMap.create(initializationOptions);
            mCanvas.setTexture(new Texture(mBitmap));
        }
        // Set pointer canvas
        if (mPointerBitmap == null || mPointerBitmap.isReleased()) {
            mPointerBitmap = PixelMap.create(initializationOptions);
            mPointerCanvas.setTexture(new Texture(mPointerBitmap));
        }
        // Set icons canvas
        if (mIconsBitmap == null || mIconsBitmap.isReleased()) {
            mIconsBitmap = PixelMap.create(initializationOptions);
            mIconsCanvas.setTexture(new Texture(mIconsBitmap));
        }
        // Set titles canvas
        if (mIsTitled) {
            if (mTitlesBitmap == null || mTitlesBitmap.isReleased()) {
                mTitlesBitmap = PixelMap.create(initializationOptions);
                mTitlesCanvas.setTexture(new Texture(mTitlesBitmap));
            }
        } else mTitlesBitmap = null;

        // Reset and clear canvases
        mCanvas.drawColor(0, CLEAR);
        mPointerCanvas.drawColor(0, CLEAR);
        mIconsCanvas.drawColor(0, CLEAR);
        if (mIsTitled) mTitlesCanvas.drawColor(0, CLEAR);

        //This is background gray color
        if (mCornersRadius == 0) canvas.drawRect(mBgBounds, mBgPaint);
        else canvas.drawRoundRect(mBgBounds, mCornersRadius, mCornersRadius, mBgPaint);

        // Get pointer badge margin for gravity
        final float barBadgeMargin = mBadgeGravity == BadgeGravity.TOP ? mBadgeMargin : 0.0F;

        // Draw our model colors
        for (int i = 0; i < mModels.size(); i++) {
            mPaint.setColor(new Color(mModels.get(i).getColor()));

            if (mIsHorizontalOrientation) {
                final float left = mModelSize * i;
                final float right = left + mModelSize;
                mCanvas.drawRect(new RectF(left, barBadgeMargin, right, mBounds.height() + barBadgeMargin),
                        mPaint);
            } else {
                final float top = mModelSize * i;
                final float bottom = top + mModelSize;
                mCanvas.drawRect(new RectF(0.0F, top, mBounds.width(), bottom), mPaint);
            }
        }

        // Set bound of pointer
        if (mIsHorizontalOrientation) mPointerBounds.set(new RectF(
                mPointerLeftTop,
                barBadgeMargin,
                mPointerRightBottom,
                mBounds.height() + barBadgeMargin
        ));
        else mPointerBounds.set(new RectF(0.0F, mPointerLeftTop, mBounds.width(), mPointerRightBottom));

        // Draw pointer for model colors
        //This is to draw highlighed big icon
        if (mCornersRadius == 0) mPointerCanvas.drawRect(mPointerBounds, mPaint);
        else mPointerCanvas.drawRoundRect(mPointerBounds, mCornersRadius, mCornersRadius, mPaint);

        // Draw pointer into main canvas
        mCanvas.drawPixelMapHolder(new PixelMapHolder(mPointerBitmap), 0.0F, 0.0F, mPointerPaint);

        // Set vars for icon when model with title or without
        final float iconMarginTitleHeight = mIconSize + mTitleMargin + mModelTitleSize;

        // Draw model icons
        for (int i = 0; i < mModels.size(); i++) {
            final Model model = mModels.get(i);

            // Variables to center our icons
            final float leftOffset;
            final float topOffset;
            final float matrixCenterX;
            final float matrixCenterY;

            // Set offset to titles
            final float leftTitleOffset;
            final float topTitleOffset;
            int width = model.mIcon.getImageInfo().size.width;
            int height = model.mIcon.getImageInfo().size.height;
            if (mIsHorizontalOrientation) {
                leftOffset = (mModelSize * i) + (mModelSize - width) * 0.5F;
                topOffset = (mBounds.height() - height) * 0.5F;

                // Set offset to titles
                leftTitleOffset = (mModelSize * i) + (mModelSize * 0.5F);
                topTitleOffset =
                        mBounds.height() - (mBounds.height() - iconMarginTitleHeight) * 0.5F;

            } else {
                leftOffset = (mBounds.width() - (float) width) * 0.5F;
                topOffset = (mModelSize * i) + (mModelSize - (float) height) * 0.5F;

                // Set offset to titles
                leftTitleOffset = leftOffset + (float) width * 0.5F;
                topTitleOffset = topOffset + (height + iconMarginTitleHeight) * 0.5f;
            }

            matrixCenterX = leftOffset + (float) width * 0.5F;
            matrixCenterY = topOffset + (float) height * 0.5F;

            // Title translate position
            final float titleTranslate =
                    topOffset - height * TITLE_MARGIN_SCALE_FRACTION;

            // Translate icon to model center
            model.mIconMatrix.setTranslate(
                    leftOffset,
                    (mIsTitled && mTitleMode == TitleMode.ALL) ? titleTranslate : topOffset
            );

            // Get interpolated fraction for left last and current models
            final float interpolation =
                    mResizeInterpolator.getResizeInterpolation(mFraction, true);
            final float lastInterpolation =
                    mResizeInterpolator.getResizeInterpolation(mFraction, false);

            // Scale value relative to interpolation
            final float matrixScale = model.mActiveIconScaleBy * interpolation;
            final float matrixLastScale = model.mActiveIconScaleBy * lastInterpolation;

            // Get title alpha relative to interpolation
            final int titleAlpha = (int) (MAX_ALPHA * interpolation);
            final int titleLastAlpha = MAX_ALPHA - (int) (MAX_ALPHA * lastInterpolation);
            // Get title scale relative to interpolation
            final float titleScale = mIsScaled ?
                    MAX_FRACTION + interpolation * TITLE_ACTIVE_SCALE_BY : MAX_FRACTION;
            final float titleLastScale = mIsScaled ? (MAX_FRACTION + TITLE_ACTIVE_SCALE_BY) -
                    (lastInterpolation * TITLE_ACTIVE_SCALE_BY) : titleScale;

            mIconPaint.setAlpha(MAX_ALPHA);
            if (model.mSelectedIcon != null) mSelectedIconPaint.setAlpha(MAX_ALPHA);

            if (mIsSetIndexFromTabBar) {
                if (mIndex == i)
                    updateCurrentModel(
                            model,
                            leftOffset,
                            topOffset,
                            titleTranslate,
                            interpolation,
                            matrixCenterX,
                            matrixCenterY,
                            matrixScale,
                            titleScale,
                            titleAlpha
                    );
                else if (mLastIndex == i)
                    updateLastModel(
                            model,
                            leftOffset,
                            topOffset,
                            titleTranslate,
                            lastInterpolation,
                            matrixCenterX,
                            matrixCenterY,
                            matrixLastScale,
                            titleLastScale,
                            titleLastAlpha
                    );
                else
                    updateInactiveModel(
                            model,
                            leftOffset,
                            topOffset,
                            titleScale,
                            matrixScale,
                            matrixCenterX,
                            matrixCenterY
                    );
            } else {
                if (i == mIndex + 1)
                    updateCurrentModel(
                            model,
                            leftOffset,
                            topOffset,
                            titleTranslate,
                            interpolation,
                            matrixCenterX,
                            matrixCenterY,
                            matrixScale,
                            titleScale,
                            titleAlpha
                    );
                else if (i == mIndex)
                    updateLastModel(
                            model,
                            leftOffset,
                            topOffset,
                            titleTranslate,
                            lastInterpolation,
                            matrixCenterX,
                            matrixCenterY,
                            matrixLastScale,
                            titleLastScale,
                            titleLastAlpha
                    );
                else updateInactiveModel(
                            model,
                            leftOffset,
                            topOffset,
                            titleScale,
                            matrixScale,
                            matrixCenterX,
                            matrixCenterY
                    );
            }

            RectF iconMatrixRect = new RectF();
            model.mIconMatrix.mapRect(iconMatrixRect);
            // Draw original model icon
            if (model.mSelectedIcon == null) {
                if (model.mIcon != null && !model.mIcon.isReleased()){
                    mIconsCanvas.setMatrix(model.mIconMatrix);
                    mIconsCanvas.drawPixelMapHolder(new PixelMapHolder(model.mIcon), 0,0, mIconPaint);
                }
            } else if (mIconPaint.getAlpha() != MIN_ALPHA &&
                    model.mIcon != null && !model.mIcon.isReleased()){
                // Draw original icon when is visible
                mIconsCanvas.setMatrix(model.mIconMatrix);
                mIconsCanvas.drawPixelMapHolder(new PixelMapHolder(model.mIcon), 0,0, mIconPaint);
            }

            // Draw selected icon when exist and visible
            if (mSelectedIconPaint.getAlpha() != MIN_ALPHA &&
                    model.mSelectedIcon != null && !model.mSelectedIcon.isReleased()){
                mIconsCanvas.setMatrix(model.mIconMatrix);
                mIconsCanvas.drawPixelMapHolder(new PixelMapHolder(model.mIcon), 0,0, mIconPaint);
            }

            if (mIsTitled) mTitlesCanvas.drawText(
                    mModelTitlePaint,
                    model.getTitle(),
                    leftTitleOffset,
                    topTitleOffset

            );
        }

        // Reset pointer bounds for icons and titles
        if (mIsHorizontalOrientation)
            mPointerBounds.set(new RectF(mPointerLeftTop, 0.0F, mPointerRightBottom, mBounds.height()));
        if (mCornersRadius == 0) {
            if (mIsTinted) mIconsCanvas.drawRect(mPointerBounds, mIconPointerPaint);
            if (mIsTitled) mTitlesCanvas.drawRect(mPointerBounds, mIconPointerPaint);
        } else {
            if (mIsTinted) mIconsCanvas.drawRoundRect(
                    mPointerBounds, mCornersRadius, mCornersRadius, mIconPointerPaint
            );
            if (mIsTitled) mTitlesCanvas.drawRoundRect(
                    mPointerBounds, mCornersRadius, mCornersRadius, mIconPointerPaint
            );
        }

        // Draw general bitmap
        canvas.drawPixelMapHolder(new PixelMapHolder(mBitmap), 0.0F, 0.0F, mDefaultPaint);
        // Draw icons bitmap on top
        canvas.drawPixelMapHolder(new PixelMapHolder(mIconsBitmap), 0.0F, barBadgeMargin, mDefaultPaint);
        // Draw titles bitmap on top
        if (mIsTitled) canvas.drawPixelMapHolder(new PixelMapHolder(mTitlesBitmap), 0.0F, barBadgeMargin, mDefaultPaint);

        // If is not badged, exit
        if (!mIsBadged) return;

        // Model badge margin and offset relative to gravity mode
        final float modelBadgeMargin =
                mBadgeGravity == BadgeGravity.TOP ? mBadgeMargin : mBounds.height();
        final float modelBadgeOffset =
                mBadgeGravity == BadgeGravity.TOP ? 0.0F : mBounds.height() - mBadgeMargin;

        for (int i = 0; i < mModels.size(); i++) {
            final Model model = mModels.get(i);

            // Set preview badge title
            if (isEmpty(model.getBadgeTitle()))
                model.setBadgeTitle(PREVIEW_BADGE);

            // Set badge title bounds
            mBadgePaint.setTextSize((int) (mBadgeTitleSize * model.mBadgeFraction));
            Rect rect = mBadgePaint.getTextBounds(model.getBadgeTitle());
            mBadgeBounds.set(rect.left, rect.top, rect.right, rect.bottom);

            // Get horizontal and vertical padding for bg
            final float horizontalPadding = mBadgeTitleSize * BADGE_HORIZONTAL_FRACTION;
            final float verticalPadding = horizontalPadding * BADGE_VERTICAL_FRACTION;

            // Set horizontal badge offset
            final float badgeBoundsHorizontalOffset =
                    (mModelSize * i) + (mModelSize * mBadgePosition.mPositionFraction);

            // If is badge title only one char, so create circle else round rect
            final float badgeMargin = mBadgeMargin * model.mBadgeFraction;
            if (model.getBadgeTitle().length() == 1) {
                mBgBadgeBounds.set(new RectF(
                        badgeBoundsHorizontalOffset - badgeMargin,
                        modelBadgeMargin - badgeMargin,
                        badgeBoundsHorizontalOffset + badgeMargin,
                        modelBadgeMargin + badgeMargin
                ));
            } else
                mBgBadgeBounds.set(new RectF(
                        badgeBoundsHorizontalOffset -
                                Math.max(badgeMargin, mBadgeBounds.getCenterX() + horizontalPadding),
                        modelBadgeMargin - badgeMargin,
                        badgeBoundsHorizontalOffset +
                                Math.max(badgeMargin, mBadgeBounds.getCenterX() + horizontalPadding),
                        modelBadgeOffset + (verticalPadding * 2.0F) + mBadgeBounds.getHeight()
                ));

            // Set color and alpha for badge bg
            if (model.mBadgeFraction == MIN_FRACTION) mBadgePaint.setColor(Color.TRANSPARENT);
            else mBadgePaint.setColor(new Color(mBadgeBgColor == AUTO_COLOR ? mActiveColor : mBadgeBgColor));
            mBadgePaint.setAlpha((int) (MAX_ALPHA * model.mBadgeFraction));

            // Set corners to round rect for badge bg and draw
            final float cornerRadius = mBgBadgeBounds.height() * 0.5F;
            canvas.drawRoundRect(mBgBadgeBounds, cornerRadius, cornerRadius, mBadgePaint);

            // Set color and alpha for badge title
            if (model.mBadgeFraction == MIN_FRACTION) mBadgePaint.setColor(Color.TRANSPARENT);
            else //noinspection ResourceAsColor
                mBadgePaint.setColor(new Color(
                        mBadgeTitleColor == AUTO_COLOR ? model.getColor() : mBadgeTitleColor));
            mBadgePaint.setAlpha((int) (MAX_ALPHA * model.mBadgeFraction));

            // Set badge title center position and draw title
            final float badgeHalfHeight = mBadgeBounds.getHeight() * 0.5F;
            float badgeVerticalOffset = (mBgBadgeBounds.height() * 0.5F) + badgeHalfHeight -
                    mBadgeBounds.bottom + modelBadgeOffset;
            canvas.drawText(mBadgePaint,
                    model.getBadgeTitle(), badgeBoundsHorizontalOffset, badgeVerticalOffset +
                            mBadgeBounds.getHeight() - (mBadgeBounds.getHeight() * model.mBadgeFraction)

            );
        }
    }

    // Method to transform current fraction of NTB and position
    protected void updateCurrentModel(
            final Model model,
            final float leftOffset,
            final float topOffset,
            final float titleTranslate,
            final float interpolation,
            final float matrixCenterX,
            final float matrixCenterY,
            final float matrixScale,
            final float titleScale,
            final int titleAlpha
    ) {
        if (mIsTitled && mTitleMode == TitleMode.ACTIVE) model.mIconMatrix.setTranslate(
                leftOffset, topOffset - (interpolation * (topOffset - titleTranslate))
        );

        final float scale = model.mInactiveIconScale + (mIsScaled ? matrixScale : 0.0F);
        model.mIconMatrix.postScale(scale, scale, matrixCenterX, matrixCenterY);

        mModelTitlePaint.setTextSize((int) (mModelTitleSize * titleScale));
        if (mTitleMode == TitleMode.ACTIVE) mModelTitlePaint.setAlpha(titleAlpha);

        if (model.mSelectedIcon == null) {
            mIconPaint.setAlpha(MAX_ALPHA);
            return;
        }

        // Calculate cross fade alpha between icon and selected icon
        final float iconAlpha;
        final float selectedIconAlpha;
        if (interpolation <= 0.475F) {
            iconAlpha = MAX_FRACTION - interpolation * 2.1F;
            selectedIconAlpha = MIN_FRACTION;
        } else if (interpolation >= 0.525F) {
            iconAlpha = MIN_FRACTION;
            selectedIconAlpha = (interpolation - 0.55F) * 1.9F;
        } else {
            iconAlpha = MIN_FRACTION;
            selectedIconAlpha = MIN_FRACTION;
        }

        mIconPaint.setAlpha((int) (MAX_ALPHA * clampValue(iconAlpha)));
        mSelectedIconPaint.setAlpha((int) (MAX_ALPHA * clampValue(selectedIconAlpha)));
    }

    // Method to transform last fraction of NTB and position
    protected void updateLastModel(
            final Model model,
            final float leftOffset,
            final float topOffset,
            final float titleTranslate,
            final float lastInterpolation,
            final float matrixCenterX,
            final float matrixCenterY,
            final float matrixLastScale,
            final float titleLastScale,
            final int titleLastAlpha
    ) {
        if (mIsTitled && mTitleMode == TitleMode.ACTIVE) model.mIconMatrix.setTranslate(
                leftOffset, titleTranslate + (lastInterpolation * (topOffset - titleTranslate))
        );

        final float scale = model.mInactiveIconScale +
                (mIsScaled ? model.mActiveIconScaleBy - matrixLastScale : 0.0F);
        model.mIconMatrix.postScale(scale, scale, matrixCenterX, matrixCenterY);

        mModelTitlePaint.setTextSize((int) (mModelTitleSize * titleLastScale));
        if (mTitleMode == TitleMode.ACTIVE) mModelTitlePaint.setAlpha(titleLastAlpha);

        if (model.mSelectedIcon == null) {
            mIconPaint.setAlpha(MAX_ALPHA);
            return;
        }

        // Calculate cross fade alpha between icon and selected icon
        final float iconAlpha;
        final float selectedIconAlpha;
        if (lastInterpolation <= 0.475F) {
            iconAlpha = MIN_FRACTION;
            selectedIconAlpha = MAX_FRACTION - lastInterpolation * 2.1F;
        } else if (lastInterpolation >= 0.525F) {
            iconAlpha = (lastInterpolation - 0.55F) * 1.9F;
            selectedIconAlpha = MIN_FRACTION;
        } else {
            iconAlpha = MIN_FRACTION;
            selectedIconAlpha = MIN_FRACTION;
        }

        mIconPaint.setAlpha((int) (MAX_ALPHA * clampValue(iconAlpha)));
        mSelectedIconPaint.setAlpha((int) (MAX_ALPHA * clampValue(selectedIconAlpha)));
    }

    // Method to transform others fraction of NTB and position
    protected void updateInactiveModel(
            final Model model,
            final float leftOffset,
            final float topOffset,
            final float textScale,
            final float matrixScale,
            final float matrixCenterX,
            final float matrixCenterY
    ) {
        if (mIsTitled && mTitleMode == TitleMode.ACTIVE)
            model.mIconMatrix.setTranslate(leftOffset, topOffset);
        model.mIconMatrix.postScale(
                model.mInactiveIconScale, model.mInactiveIconScale, matrixCenterX, matrixCenterY
        );

        mModelTitlePaint.setTextSize((int) mModelTitleSize);
        if (mTitleMode == TitleMode.ACTIVE) mModelTitlePaint.setAlpha(MIN_ALPHA);

        // Reset icons alpha
        if (model.mSelectedIcon == null) {
            mIconPaint.setAlpha(MAX_ALPHA);
            return;
        }

        mSelectedIconPaint.setAlpha(MIN_ALPHA);
    }

    private float prevPosition = 0;
    @Override
    public void onPageSliding(int position, float positionOffset, final int positionOffsetPixels) {
        if (positionOffset == 1f) {
            positionOffset = 0f;
        }

        if (mOnPageChangeListener != null)
            mOnPageChangeListener.onPageSliding(position, positionOffset, positionOffsetPixels);

        // If we animate, don`t call this
        if (!mIsSetIndexFromTabBar) {
            mIsResizeIn = position < mIndex;
            mLastIndex = mIndex;
            mIndex = position;

            mStartPointerX = position * mModelSize;
            mEndPointerX = mStartPointerX + mModelSize;
            updateIndicatorPosition(positionOffset);
        }

        // Stop scrolling on animation end and reset values
        if (!mAnimator.isRunning() && mIsSetIndexFromTabBar) {
            mFraction = MIN_FRACTION;
            mIsSetIndexFromTabBar = false;
        }
    }

    @Override
    public void onPageSlideStateChanged(int state) {
        // If VP idle, reset to MIN_FRACTION
        mScrollState = state;
        if (state == PageSlider.SLIDING_STATE_IDLE) {
            if (mOnPageChangeListener != null) mOnPageChangeListener.onPageChosen(mIndex);
            if (mIsViewPagerMode && mOnTabBarSelectedIndexListener != null)
                mOnTabBarSelectedIndexListener.onEndTabSelected(mModels.get(mIndex), mIndex);
        }

        if (mOnPageChangeListener != null) mOnPageChangeListener.onPageSlideStateChanged(state);
    }

    @Override
    public void onPageChosen(int i) {
        // This method is empty, because we call onPageChosen() when scroll state is idle
    }

    // Clamp value to max and min bounds
    protected float clampValue(final float value) {
        return Math.max(
                Math.min(value, NavigationTabBar.MAX_FRACTION), NavigationTabBar.MIN_FRACTION
        );
    }

    // Hide NTB or bg on scroll down
    protected void scrollDown() {
        AnimatorProperty animatorProperty = createAnimatorProperty();
        animatorProperty.moveToY(getBarHeight())
                .setCurveType(Animator.CurveType.LINEAR)
                .setDuration(DEFAULT_ANIMATION_DURATION)
                .start();
    }

    // Show NTB or bg on scroll up
    protected void scrollUp() {
        AnimatorProperty animatorProperty = createAnimatorProperty();
        animatorProperty.moveToY(0.0F)
                .setCurveType(Animator.CurveType.LINEAR)
                .setDuration(DEFAULT_ANIMATION_DURATION)
                .start();
    }

    // Model class
    public static class Model {

        private int mColor;

        private final PixelMap mIcon;
        private final PixelMap mSelectedIcon;
        private final Matrix mIconMatrix = new Matrix();

        private String mTitle = "";
        private String mBadgeTitle = "";
        private String mTempBadgeTitle = "";
        private float mBadgeFraction;

        private boolean mIsBadgeShowed;
        private boolean mIsBadgeUpdated;

        private final AnimatorValue mBadgeAnimator = new AnimatorValue();

        private float mInactiveIconScale;
        private float mActiveIconScaleBy;

        Model(final Builder builder) {
            mColor = builder.mColor;
            mIcon = builder.mIcon;
            mSelectedIcon = builder.mSelectedIcon;
            mTitle = builder.mTitle;
            mBadgeTitle = builder.mBadgeTitle;


            mBadgeAnimator.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                    ((AnimatorValue) animator).setStateChangedListener(null);
                    ((AnimatorValue) animator).setStateChangedListener(this);
                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    ((AnimatorValue) animator).setStateChangedListener(null);
                    ((AnimatorValue) animator).setStateChangedListener(this);

                    // Detect whether we just update text and don`t reset show state
                    if (!mIsBadgeUpdated) mIsBadgeShowed = !mIsBadgeShowed;
                    else mIsBadgeUpdated = false;
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });

            mBadgeAnimator.setLoopedListener(new Animator.LoopedListener() {
                @Override
                public void onRepeat(Animator animator) {
                    // Change title when we update and don`t see the title
                    if (mIsBadgeUpdated) mBadgeTitle = mTempBadgeTitle;
                }
            });
        }

        public String getTitle() {
            return mTitle;
        }

        public void setTitle(final String title) {
            mTitle = title;
        }

        public int getColor() {
            return mColor;
        }

        public void setColor(final int color) {
            mColor = color;
        }

        public boolean isBadgeShowed() {
            return mIsBadgeShowed;
        }

        public String getBadgeTitle() {
            return mBadgeTitle;
        }

        public void setBadgeTitle(final String badgeTitle) {
            mBadgeTitle = badgeTitle;
        }

        // If your badge is visible on screen, so you can update title with animation
        public void updateBadgeTitle(final String badgeTitle) {
            if (!mIsBadgeShowed) return;
            if (mBadgeAnimator.isRunning()) mBadgeAnimator.end();

            mTempBadgeTitle = badgeTitle;
            mIsBadgeUpdated = true;

            mBadgeAnimator.setDuration(DEFAULT_BADGE_REFRESH_ANIMATION_DURATION);
            mBadgeAnimator.setLoopedCount(1);
            mBadgeAnimator.start();
        }

        public void toggleBadge() {
            if (mBadgeAnimator.isRunning()) mBadgeAnimator.end();
            if (mIsBadgeShowed) hideBadge();
            else showBadge();
        }

        public void showBadge() {
            mIsBadgeUpdated = false;

            if (mBadgeAnimator.isRunning()) mBadgeAnimator.end();
            if (mIsBadgeShowed) return;

            mBadgeAnimator.setCurveType(Animator.CurveType.DECELERATE);
            mBadgeAnimator.setDuration(DEFAULT_BADGE_ANIMATION_DURATION);
            mBadgeAnimator.setLoopedCount(0);
            mBadgeAnimator.start();
        }

        public void hideBadge() {
            mIsBadgeUpdated = false;

            if (mBadgeAnimator.isRunning()) mBadgeAnimator.end();
            if (!mIsBadgeShowed) return;

            mBadgeAnimator.setCurveType(Animator.CurveType.ACCELERATE);
            mBadgeAnimator.setDuration(DEFAULT_BADGE_ANIMATION_DURATION);
            mBadgeAnimator.setLoopedCount(0);
            mBadgeAnimator.start();
        }

        public static class Builder {

            private final int mColor;

            private final PixelMap mIcon;
            private PixelMap mSelectedIcon;

            private String mTitle;
            private String mBadgeTitle;

            public Builder(final Element icon, final int color) {
                PixelMap pixelMap;
                mColor = color;
                PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
                if (icon != null) {
                    if (icon instanceof PixelMapElement) pixelMap = ((PixelMapElement) icon).getPixelMap();
                    else {
                        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
                        int width = Math.max(icon.getBounds().getWidth(), 2);
                        int height = Math.max(icon.getBounds().getHeight(), 2);
                        initializationOptions.size = new Size(width, height);
                        try {
                            pixelMap = PixelMap.create(initializationOptions);
                            final Canvas canvas = new Canvas();
                            canvas.drawPixelMapHolder(new PixelMapHolder(pixelMap), 0, 0, mDefaultPaint);
                            icon.drawToCanvas(canvas);
                        } catch (Throwable e) {
                            e.printStackTrace();
                            LogUtil.info(TAG, "Failed to create bitmap from drawable!");
                            pixelMap = null;
                        }
                    }
                } else {
                    initializationOptions.pixelFormat = PixelFormat.RGB_565;
                    initializationOptions.size = new Size(1, 1);
                    pixelMap = PixelMap.create(initializationOptions);
                }
                mIcon = pixelMap;
            }

            public Builder selectedIcon(final Element selectedIcon) {
                PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
                if (selectedIcon != null) {
                    if (selectedIcon instanceof PixelMapElement)
                        mSelectedIcon = ((PixelMapElement) selectedIcon).getPixelMap();
                    else {
                        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
                        int width = Math.max(selectedIcon.getBounds().getWidth(), 2);
                        int height = Math.max(selectedIcon.getBounds().getHeight(), 2);
                        initializationOptions.size = new Size(width, height);
                        try {
                            mSelectedIcon = PixelMap.create(initializationOptions);
                            final Canvas canvas = new Canvas();
                            canvas.drawPixelMapHolder(new PixelMapHolder(mSelectedIcon), 0, 0, mDefaultPaint);
                            selectedIcon.drawToCanvas(canvas);
                        } catch (Throwable e) {
                            e.printStackTrace();
                            LogUtil.info(TAG, "Failed to create bitmap from drawable!");
                            mSelectedIcon = null;
                        }
                    }
                } else mSelectedIcon = null;

                return this;
            }

            public Builder title(final String title) {
                mTitle = title;
                return this;
            }

            public Builder badgeTitle(final String title) {
                mBadgeTitle = title;
                return this;
            }

            public Model build() {
                return new Model(this);
            }
        }
    }

    // Resize interpolator to create smooth effect on pointer according to inspiration design
    // This is like improved accelerated and decelerated interpolator
    protected class ResizeInterpolator {

        // Spring factor
        private final static float FACTOR = 1.0F;
        // Check whether side we move
        private boolean mResizeIn;

        public float getInterpolation(final float input) {
            if (mResizeIn) return (float) (1.0F - Math.pow((1.0F - input), 2.0F * FACTOR));
            else return (float) (Math.pow(input, 2.0F * FACTOR));
        }

        private float getResizeInterpolation(final float input, final boolean resizeIn) {
            mResizeIn = resizeIn;
            return getInterpolation(input);
        }
    }

    // Model title mode
    public enum TitleMode {
        ALL, ACTIVE;

        public final static int ALL_INDEX = 0;
        public final static int ACTIVE_INDEX = 1;
    }

    // Model badge position
    public enum BadgePosition {
        LEFT(LEFT_FRACTION), CENTER(CENTER_FRACTION), RIGHT(RIGHT_FRACTION);

        public final static int LEFT_INDEX = 0;
        public final static int CENTER_INDEX = 1;
        public final static int RIGHT_INDEX = 2;

        private final float mPositionFraction;

        BadgePosition(final float positionFraction) {
            mPositionFraction = positionFraction;
        }
    }

    // Model badge gravity
    public enum BadgeGravity {
        TOP, BOTTOM;

        public final static int TOP_INDEX = 0;
        public final static int BOTTOM_INDEX = 1;
    }

    // Out listener for selected index
    public interface OnTabBarSelectedIndexListener {
        void onStartTabSelected(final Model model, final int index);

        void onEndTabSelected(final Model model, final int index);
    }
}
