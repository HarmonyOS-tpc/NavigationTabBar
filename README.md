## NavigationTabBar

## Introduction
NavigationTabBar : Navigation tab bar for openharmony with colorful interactions.

### Features

- Allows you to set NTB models, where you set icon and color. Can be set up only via code.
- Allows you to connect NTB with ViewPager. If you want your can also set OnPageChangeListener.
- Allows you to set background to NTB which automatically set with offset relative to badge gravity and corners radius.
- Allows you to set selected icon when current model is active.
- Allows you to enable title in you model.
- Allows you to enable badge in you model.
- Allows you to handle mode of the model title show. Can show all or only active.
- Allows you to set titles size.
- Allows you to enable badge in you model.
- Allows you to handle set of custom typeface in your badge.
- Allows you to handle mode of the model title show. Can show all or only active.
- Allows you to set titles size.
- Allows you to handle mode of the model icon and title scale.
- Allows you to enable or disable icon tinting.
- Allows you to set badges size.
- Allows you to set the badge position in you model. Can be: left(25%), center(50%) and right(75%).
- Allows you to set the badge gravity in NTB. Can be top or bottom.
- Allows you to set the badge bg and title colors.
- Allows you to set custom typeface to your title.
- Allows you to set corners radius of pointer.
- Allows you to set icon size fraction relative to smaller model side.
- Allows you to set animation duration.
- Allows you to set inactive icon color.
- Allows you to set active icon color.
- Allows you to set listener which triggering on start or on end when you set model index.
- Allows you to set preview colors, which generate count of models equals to count of colors.

## Usage Instructions

```XML

final NavigationTabBar mNavigationTabBar = (NavigationTabBar) rootLayout.findComponentById(ResourceTable.Id_ntb_horizontal);
final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
            models.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_first, "image/png")),
                            Color.getIntColor(colors[0]))
                            .selectedIcon(new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_sixth, "image/png")))
                            .title("Heart")
                            .badgeTitle("NTB")
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_second, "image/png")),
                            Color.getIntColor(colors[1]))
                            .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                            .title("Cup")
                            .badgeTitle("with")
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_third, "image/png")),
                            Color.getIntColor(colors[2]))
                            .selectedIcon(new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_seventh, "image/png")))
                            .title("Dips")
                            .badgeTitle("state")
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fourth, "image/png")),
                            Color.getIntColor(colors[3]))
                            .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                            .title("Flag")
                            .badgeTitle("icon")
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fifth, "image/png")),
                            Color.getIntColor(colors[4]))
                            .selectedIcon(new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_eighth, "image/png")))
                            .title("Medal")
                            .badgeTitle("777")
                            .build()
            );

            mNavigationTabBar.setModels(models);
            mNavigationTabBar.setViewPager(viewPager, 2);

            mNavigationTabBar.setTitleMode(NavigationTabBar.TitleMode.ACTIVE);
            mNavigationTabBar.setBadgeGravity(NavigationTabBar.BadgeGravity.BOTTOM);
            mNavigationTabBar.setBadgePosition(NavigationTabBar.BadgePosition.CENTER);
            mNavigationTabBar.setTypeface("fonts/custom_font.ttf");
            mNavigationTabBar.setIsBadged(true);
            mNavigationTabBar.setIsTitled(true);
            mNavigationTabBar.setIsTinted(true);
            mNavigationTabBar.setIsBadgeUseTypeface(true);
            mNavigationTabBar.setBadgeBgColor(Color.RED);
            mNavigationTabBar.setBadgeTitleColor(Color.WHITE);
            mNavigationTabBar.setIsSwiped(true);
            mNavigationTabBar.setBgColor(Color.BLACK);
            mNavigationTabBar.setBadgeSize(10);
            mNavigationTabBar.setTitleSize(10);
            mNavigationTabBar.setIconSizeFraction(0.5);

<devlight.io.library.ntb.NavigationTabBar
   ohos:id="@+id/ntb"
   ohos:layout_width="match_parent"
   ohos:layout_height="50dp"
   ohos:ntb_animation_duration="400"
   ohos:ntb_preview_colors="@array/colors"
   ohos:ntb_corners_radius="10dp"
   ohos:ntb_active_color="#fff"
   ohos:ntb_inactive_color="#000"
   ohos:ntb_badged="true"
   ohos:ntb_titled="true"
   ohos:ntb_scaled="true"
   ohos:ntb_tinted="true"
   ohos:ntb_title_mode="all"
   ohos:ntb_badge_position="right"
   ohos:ntb_badge_gravity="top"
   ohos:ntb_badge_bg_color="#ffff0000"
   ohos:ntb_badge_title_color="#ffffffff"
   ohos:ntb_typeface="fonts/custom_typeface.ttf"
   ohos:ntb_badge_use_typeface="true"
   ohos:ntb_swiped="true"
   ohos:ntb_bg_color="#000"
   ohos:ntb_icon_size_fraction="0.5"
   ohos:ntb_badge_size="10sp"
   ohos:ntb_title_size="10sp"/>
   
```


## Installation Instructions

```

Method 1: Generate har package from library and add it to lib folder.
       add following code to gradle of entry
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
Method 2:
    allprojects{
        repositories{
            mavenCentral()
        }
    }
    implementation 'io.openharmony.tpc.thirdlib:navigationtabbar:1.0.0'
```

## Version Iteration

v1.0.0

## License

```
Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "{}"
      replaced with your own identifying information. (Don't include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright {2016} {Basil Miller}

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

MIT License (MIT)

    Copyright (c) 2016 Basil Miller

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
```