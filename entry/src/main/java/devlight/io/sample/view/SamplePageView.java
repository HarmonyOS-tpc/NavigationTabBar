/*
 * Copyright (C) 2015 Basil Miller
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package devlight.io.sample.view;

import devlight.io.library.ResUtil;
import devlight.io.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.ShapeElement;

public class SamplePageView extends AbstractPageView {

    private String TAG = SamplePageView.class.getSimpleName();

    public SamplePageView(AbilitySlice abilitySlice,String name) {
       super(abilitySlice.getContext(), name);
    }
    @Override
    public void initView() {
        super.setRootView(loadView());
    }

    private Component loadView(){
        ComponentContainer layout = (ComponentContainer) LayoutScatter.getInstance(getContext()).
                parse(ResourceTable.Layout_page_one, null, false);

        layout.setBackground(ResUtil.getShapeElement
                (getContext(), ShapeElement.RECTANGLE, ResourceTable.Color_material_light_brown, 5.0f));

        Text txt1 = (Text) layout.findComponentById(ResourceTable.Id_txt1);
        Text txt2 = (Text) layout.findComponentById(ResourceTable.Id_txt2);
        Text txt3 = (Text) layout.findComponentById(ResourceTable.Id_txt3);
        Text txt4 = (Text) layout.findComponentById(ResourceTable.Id_txt4);
        txt4.setText(getName());

        txt1.setBackground(ResUtil.getShapeElement
                (getContext(), ShapeElement.RECTANGLE, ResourceTable.Color_material_brown, 5.0f));
        txt2.setBackground(ResUtil.getShapeElement
                (getContext(), ShapeElement.RECTANGLE, ResourceTable.Color_material_brown, 5.0f));
        txt3.setBackground(ResUtil.getShapeElement
                (getContext(), ShapeElement.RECTANGLE, ResourceTable.Color_material_brown, 5.0f));
        txt4.setBackground(ResUtil.getShapeElement
                (getContext(), ShapeElement.RECTANGLE, ResourceTable.Color_material_brown, 5.0f));
        return layout;
    }
}
