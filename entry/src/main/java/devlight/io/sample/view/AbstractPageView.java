/*
 * Copyright (C) 2015 Basil Miller
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package devlight.io.sample.view;

import devlight.io.library.LogUtil;
import ohos.agp.components.Component;
import ohos.app.Context;

public abstract class AbstractPageView {

    private Component rootView;
    private String name;
    protected Context context;
    private static final String TAG = AbstractPageView.class.getCanonicalName();

    public AbstractPageView(Context context, String name) {
        if (context != null) {
            this.context = context;
        } else {
            LogUtil.error(TAG, "null context");
        }
        this.name = name;
        initView();
    }

    public abstract void initView();

    public Context getContext() {
        return this.context;
    }

    public String getName() {
        return this.name;
    }

    public Component getRootView() {
        return this.rootView;
    }

    public void setRootView(Component component) {
        this.rootView = component;
    }
}
