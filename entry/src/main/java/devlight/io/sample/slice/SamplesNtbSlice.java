/*
 * Copyright (C) 2015 Basil Miller
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package devlight.io.sample.slice;

import devlight.io.library.LogUtil;
import devlight.io.library.NavigationTabBar;
import devlight.io.library.ResUtil;
import devlight.io.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

import java.util.ArrayList;

import static devlight.io.library.ResUtil.createByResourceId;

public class SamplesNtbSlice extends AbilitySlice {

    NavigationTabBar ntbSample1,ntbSample2,ntbSample3,ntbSample4,ntbSample5,ntbSample6;
    String TAG = SamplesNtbSlice.class.getSimpleName();
    private static final String mBgColor = "#423752";
    private static final float mRadius = 5.0f;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_sample_ntb, null, false);
        rootLayout.setBackground(ResUtil.getShapeElement
                (this, ShapeElement.RECTANGLE, ResourceTable.Color_material_brown, mRadius));
        ntbSample1 = (NavigationTabBar) rootLayout.findComponentById(ResourceTable.Id_ntb_sample_1);
        ntbSample2 = (NavigationTabBar) rootLayout.findComponentById(ResourceTable.Id_ntb_sample_2);
        ntbSample3 = (NavigationTabBar) rootLayout.findComponentById(ResourceTable.Id_ntb_sample_3);
        ntbSample4 = (NavigationTabBar) rootLayout.findComponentById(ResourceTable.Id_ntb_sample_4);
        ntbSample5 = (NavigationTabBar) rootLayout.findComponentById(ResourceTable.Id_ntb_sample_5);
        ntbSample6 = (NavigationTabBar) rootLayout.findComponentById(ResourceTable.Id_ntb_sample_6);
        initUI();
        setUIContent(rootLayout);
    }

    private void initUI() {
        ArrayList<NavigationTabBar.Model> models1 = new ArrayList<>();
        String[] colors = ResUtil.getStringArray(this, ResourceTable.Strarray_default_preview);
        try {
            models1.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_first, "image/png")), Color.getIntColor(colors[0]))
                            .build()
            );
            models1.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_second, "image/png")), Color.getIntColor(colors[1])
                    ).build()
            );
            models1.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_third, "image/png")), Color.getIntColor(colors[2])
                    ).build()
            );
            models1.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fourth, "image/png")), Color.getIntColor(colors[3])
                    ).build()
            );
        } catch (Exception e) {
            LogUtil.warn(TAG, "Exception in initNTB : " + e.toString());
        }
        ntbSample1.setModels(models1);

        ArrayList<NavigationTabBar.Model> models2 = new ArrayList<>();
        try {
            models2.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_first, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
            models2.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_second, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
            models2.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_third, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
            models2.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_second, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
            models2.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fourth, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
        } catch (Exception e) {
            LogUtil.warn(TAG, "Exception in initUI : " + e.toString());
        }

        ntbSample2.setModels(models2);
        ntbSample2.setModelIndex(3, true);

        ArrayList<NavigationTabBar.Model> models3 = new ArrayList<>();
        try {
            models3.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fourth, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
            models3.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fourth, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
            models3.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fourth, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
        } catch (Exception e) {
            LogUtil.warn(TAG, "Exception in initUI : " + e.toString());
        }

        ntbSample3.setModels(models3);
        ntbSample3.setModelIndex(1, true);

        ArrayList<NavigationTabBar.Model> models4 = new ArrayList<>();
        int bgColor = Color.getIntColor(mBgColor);
        try {
            models4.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_first, "image/png")), bgColor
                    ).build()
            );
            models4.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_second, "image/png")), bgColor
                    ).build()
            );
            models4.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_third, "image/png")), bgColor
                    ).build()
            );
            models4.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fourth, "image/png")), bgColor
                    ).build()
            );
            models4.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fifth, "image/png")), bgColor
                    ).build()
            );
        } catch (Exception e) {
            LogUtil.warn(TAG, "Exception in initUI : " + e.toString());
        }

        ntbSample4.setModels(models4);
        ntbSample4.setModelIndex(2, true);

        ArrayList<NavigationTabBar.Model> models5 = new ArrayList<>();
        try {
            models5.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_first, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
            models5.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_second, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
            models5.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_third, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
            models5.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fourth, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
        } catch (Exception e) {
            LogUtil.warn(TAG, "Exception in initUI : " + e.toString());
        }

        ntbSample5.setModels(models5);
        ntbSample5.setModelIndex(2, true);
        ntbSample5.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {

            }

            @Override
            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {

            }
        });

        ArrayList<NavigationTabBar.Model> models6 = new ArrayList<>();
        try {
            models6.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_first, "image/png")), Color.getIntColor(colors[0])
                    ).build()
            );
            models6.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_second, "image/png")), Color.getIntColor(colors[1])
                    ).build()
            );
            models6.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_third, "image/png")), Color.getIntColor(colors[2])
                    ).build()
            );
            models6.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fourth, "image/png")), Color.getIntColor(colors[3])
                    ).build()
            );
        } catch (Exception e) {
            LogUtil.warn(TAG, "Exception in initUI : " + e.toString());
        }

        ntbSample6.setModels(models6);

    }
}
