/*
 * Copyright (C) 2015 Basil Miller
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package devlight.io.sample.slice;

import devlight.io.library.LogUtil;
import devlight.io.library.NavigationTabBar;
import devlight.io.library.ResUtil;
import devlight.io.sample.ResourceTable;
import devlight.io.sample.view.AbstractPageView;
import devlight.io.sample.view.PageViewAdapter;
import devlight.io.sample.view.SamplePageView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.global.resource.ResourceManager;

import java.util.ArrayList;
import java.util.List;

import static devlight.io.library.ResUtil.createByResourceId;
import static devlight.io.sample.utils.Constants.mBgRadius;
import static devlight.io.sample.utils.Constants.mTitleHeart;
import static devlight.io.sample.utils.Constants.mBadgeTitleIcon;
import static devlight.io.sample.utils.Constants.mTitleDips;
import static devlight.io.sample.utils.Constants.mTitleFlag;
import static devlight.io.sample.utils.Constants.mTitleMedal;
import static devlight.io.sample.utils.Constants.mTitleCup;
import static devlight.io.sample.utils.Constants.mBadgeTitle;
import static devlight.io.sample.utils.Constants.mBadgeTitleNtb;
import static devlight.io.sample.utils.Constants.mBadgeTitleState;
import static devlight.io.sample.utils.Constants.mBadgeTitleWith;
import static devlight.io.sample.utils.Constants.mDelayHighDuration;
import static devlight.io.sample.utils.Constants.mDelayLessDuration;
import static devlight.io.sample.utils.Constants.pages;

public class HorizontalNtbSlice extends AbilitySlice {

    PageSlider mPager;
    NavigationTabBar mNavigationTabBar;
    List<AbstractPageView> mPageViews;
    private String TAG = HorizontalNtbSlice.class.getSimpleName();
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this).
                parse(ResourceTable.Layout_horizontal_ntb, null, false);
        rootLayout.setBackground(ResUtil.getShapeElement
                (this, ShapeElement.RECTANGLE, ResourceTable.Color_material_brown, mBgRadius));
        mPager = (PageSlider) rootLayout.findComponentById(ResourceTable.Id_slider);
        mNavigationTabBar = (NavigationTabBar) rootLayout.findComponentById(ResourceTable.Id_ntb_horizontal);
        initPageView(intent);
        initNTB();
        setUIContent(rootLayout);
    }

    private void initNTB() {
        try {
            String[] colors = ResUtil.getStringArray(this, ResourceTable.Strarray_default_preview);
            ResourceManager resourceManager = getResourceManager();
            final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
            models.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_first, "image/png")),
                            Color.getIntColor(colors[0]))
                            .title(mTitleHeart)
                            .badgeTitle(mBadgeTitleNtb)
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_second, "image/png")),
                            Color.getIntColor(colors[1]))
                            .title(mTitleCup)
                            .badgeTitle(mBadgeTitleWith)
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_third, "image/png")),
                            Color.getIntColor(colors[2]))
                            .title(mTitleDips)
                            .badgeTitle(mBadgeTitleState)
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fourth, "image/png")),
                            Color.getIntColor(colors[3]))
                            .title(mTitleFlag)
                            .badgeTitle(mBadgeTitleIcon)
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            new PixelMapElement(createByResourceId(this, ResourceTable.Media_ic_fifth, "image/png")),
                            Color.getIntColor(colors[4]))
                            .title(mTitleMedal)
                            .badgeTitle(mBadgeTitle)
                            .build()
            );

            mNavigationTabBar.setModels(models);
            mNavigationTabBar.setViewPager(mPager, 2);
            mNavigationTabBar.setOnPageChangeListener(new PageSlider.PageChangedListener() {
                @Override
                public void onPageSliding(int i, float v, int i1) {

                }

                @Override
                public void onPageSlideStateChanged(int i) {

                }

                @Override
                public void onPageChosen(int i) {
                    mNavigationTabBar.getModels().get(i).hideBadge();
                }
            });

            TaskDispatcher dispatcher = getUITaskDispatcher();
            dispatcher.delayDispatch(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < mNavigationTabBar.getModels().size(); i++) {
                        final NavigationTabBar.Model model = mNavigationTabBar.getModels().get(i);
                        dispatcher.delayDispatch(new Runnable() {
                            @Override
                            public void run() {
                                model.showBadge();
                            }
                        }, i * mDelayLessDuration);
                    }
                }
            }, mDelayHighDuration);

        }catch (Exception e){
            LogUtil.warn(TAG, "Exception in initNTB : " + e.toString());
        }


    }

    private void initPageView(Intent intent) {
        mPageViews = new ArrayList();
        mPageViews.add(new SamplePageView(this,pages[0]));
        mPageViews.add(new SamplePageView(this,pages[1]));
        mPageViews.add(new SamplePageView(this,pages[2]));
        mPageViews.add(new SamplePageView(this,pages[3]));
        mPageViews.add(new SamplePageView(this,pages[4]));
        PageViewAdapter pageViewAdapter = new PageViewAdapter(this,mPageViews);
        mPager.setProvider(pageViewAdapter);
        mPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {

            }
        });


    }
}

