/*
 * Copyright (C) 2015 Basil Miller
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package devlight.io.sample.utils;

public class Constants {

    public static final String mTitleHeart = "Heart";
    public static final String mTitleCup = "Cup";
    public static final String mTitleDips = "Dips";
    public static final String mTitleFlag = "Flag";
    public static final String mTitleMedal = "Medal";
    public static final String mTitleDiploma = "Diploma";

    public static final String mBadgeTitleNtb = "NTB";
    public static final String mBadgeTitleWith = "with";
    public static final String mBadgeTitleState = "state";
    public static final String mBadgeTitleIcon = "icon";
    public static final String mBadgeTitle = "777";

    public static final String[] pages ={"page1","page2","page3","page4","page5"};

    public static final long mDelayLessDuration = 100;
    public static final long mDelayHighDuration = 500;

    public static final float mBgRadius = 5.0f;
    public static final float mScaleX = 0.9f;
    public static final float mScaleY = 0.9f;
    public static final long mDuration = 200;


}
